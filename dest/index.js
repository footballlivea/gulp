(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){

const Contact = require('./contact')


class AddContact{
    constructor(selector,conServ){
        this.selector = selector;
        this.conServ = conServ; 
        this.onAdd = ()=>{}
        document.addEventListener(
            "DOMContentLoaded",
            ()=>{
                this.init();
                this.binds();
            }
        );
    }
    init(){
        this.conteiner = document.querySelector(this.selector);
        this.name = this.conteiner.querySelector('.form_add-input_name');
        this.type = this.conteiner.querySelector('.form_add-input_type');;
        this.value = this.conteiner.querySelector('.form_add-input_text');
        this.addBtn = this.conteiner.querySelector('button');
    }
    binds(){
        this.addBtn.addEventListener('click',()=>this.add());
    }
    add(){
        if(this.value.value==''||this.name.value==''||this.type.value==''){
                alert('Заполните поля ввода')
            }else{
                
            let con = new Contact(
            this.value.value,
            this.name.value,    
            this.type.value
        );
        this.conServ.addContact(con).then(r=>{
            if(r.status === "error") this.addError(r.error);
            else this.addSuccess();
        })
    }
       
    }
    addError(text){
        alert(text);
    }
    addSuccess(){
        this.clearForm();
        this.onAdd();
    }
    clearForm(){
            this.name.value=''
            this.value.value=''
    }
}
module.exports = AddContact;
},{"./contact":3}],2:[function(require,module,exports){


class ContactServices{
    getAll(){
        return fetch(ContactServices.BASE_URL,{
            method:'GET',
            headers:{
                'Authorization':'Bearer '+sessionStorage.getItem('token'),
                'Accept':'application/json',
                'Content-Type':'application/json'
            }
        })
            .then(r => r.json())
            .then(r => r.contacts)     
    }
 
    addContact(con){
        return fetch(ContactServices.BASE_URL+'/add',{
            method:'POST',
            headers:{
                'Authorization':'Bearer '+sessionStorage.getItem('token'),
                'Accept':'application/json',
                'Content-Type':'application/json'
            },
            body:JSON.stringify({
                type:con.type,
                value:con.value,
                name:con.name
            })
        }).then(r=>r.json())
    }
}
ContactServices.BASE_URL='https://mag-contacts-api.herokuapp.com/contacts';

module.exports = ContactServices;
},{}],3:[function(require,module,exports){
let activeContact = null; 
class Contact{
    constructor(value,name,type,conServ){
        this.value = value;
        this.name = name;
        this.type = type;
        this.conServ = conServ;
        document.addEventListener(
            "DOMContentLoaded",
            ()=>{
                this.binds();
            }
        );
    }
    
    binds(){
        document.addEventListener('click',(e)=>this.chouseKontact(e))
    }
    create(c){
        let conElem = document.createElement('div');
        conElem.classList.add('con');

        if(c.id==activeContact){
            conElem.classList.add('active');
        }

        conElem.dataset.index=c.id;
    
        let showInf = document.createElement('div');
        showInf.innerHTML=">";
        showInf.classList.add('showBtn');
    
        conElem.append(`${c.name}`);
        conElem.append(showInf);
        
       return conElem;
    }

    showContact(){
        document.querySelector('.contact').innerHTML='';
        let elem = this.contact.map((c)=>this.create(c));
        document.querySelector('.contact').append(...elem);
    }
    createInfo(c){
        let conElem = document.createElement('div');
        conElem.classList.add('inf');

        if(c.id==activeContact){
            conElem.classList.remove('inf');
            conElem.classList.add('activeInf');
        }

       
    
        let name = document.createElement('div');
        name.innerHTML='Name: '+c.name;
        name.classList.add('name');

        let type = document.createElement('div');
        type.innerHTML='Type: '+c.type;
        type.classList.add('type');

        let value = document.createElement('div');
        value.innerHTML='Value: '+c.value;
        value.classList.add('value');
    

        conElem.append(name,type,value);
        
       return conElem;
    }
    showInfo(){
        document.querySelector('.contentContact').innerHTML='';
        let elem = this.contact.map((c)=>this.createInfo(c));
        document.querySelector('.contentContact').append(...elem);
    }
    update(){
        this.conServ.getAll().then(c=>{          
            this.contact = c;
            this.showContact(this.contact);
            this.showInfo(this.contact);
        }); 
    }
    chouseKontact(e){
            if(!e.target.matches('.showBtn')) return;
            let index = e.target.parentNode.dataset.index;
            activeContact = index;
            this.showContact();
            this.showInfo();
        }
    }

     

module.exports = Contact;
},{}],4:[function(require,module,exports){
const User = require('./user')
const AddContact = require('./add-contact')
const ContactServices = require('./contact-serv')
const Contact = require('./contact')
const LoginForm = require('./login-form')
const RegisterForm = require('./register-form')
const UserServices = require('./user-services')


let userService = new UserServices();
let conServ = new ContactServices();
let conttact = new Contact('','','',conServ);
let registerForm = new RegisterForm('.form_reg',userService);


        registerForm.onRegister = ()=>{
            alert("Войдите через форму для фхода");
        }
        let loginForm = new LoginForm('.form_log',userService);
        loginForm.onLogin = ()=>{
            loginForm.hideS();
            conttact.update()
        }
        let addCon = new AddContact('.addContact',conServ);
        addCon.onAdd = ()=>{
           conttact.update()
        }
        
       


},{"./add-contact":1,"./contact":3,"./contact-serv":2,"./login-form":5,"./register-form":6,"./user":8,"./user-services":7}],5:[function(require,module,exports){
const User = require('./user')


class LoginForm{
    constructor(selector,userService){
        this.selector = selector;
        this.userService = userService; 
        this.onLogin = ()=>{}
        document.addEventListener(
            "DOMContentLoaded",
            ()=>{
                this.init();
                this.binds();
            }
        );
    }
    init(){
        this.form1 = document.querySelector('.unLoginScreen');
        this.form2 = document.querySelector('.LoginScreen');
        this.conteiner = document.querySelector(this.selector);
        this.loginInput = this.conteiner.querySelector('.form_log-input_login');
        this.passwordInput = this.conteiner.querySelector('.form_log-input_password');
        this.logBtn = this.conteiner.querySelector('button');
    }
    binds(){
        this.logBtn.addEventListener('click',()=>this.login());
    }
    login(){
        let user = new User(
            this.loginInput.value,
            null,
            this.passwordInput.value
        );
        this.userService.login(user).then(r=>{
            if(r.status === "error") this.loginError(r.error); 
            else sessionStorage.clear(); sessionStorage.setItem('token',r.token); this.loginSuccess();  
        })
    }
    loginError(text){
        alert(text);
        window.location.reload();
    }
    loginSuccess(){
        this.clearForm();
        this.onLogin();
    }
    clearForm(){
            this.loginInput.value=''
            this.passwordInput.value=''
    }
    hideS(){
        this.form1.style.display='none'
        this.form2.style.display='block'
    }
}

module.exports = LoginForm;
},{"./user":8}],6:[function(require,module,exports){
const User = require('./user')



class RegisterForm{
    constructor(selector,userService){
        this.selector = selector;
        this.userService = userService; 
        this.onRegister = ()=>{}
        document.addEventListener(
            "DOMContentLoaded",
            ()=>{
                this.init();
                this.binds();
            }
        );
    }
    init(){
        this.conteiner = document.querySelector(this.selector);
        this.loginInput = this.conteiner.querySelector('.form_reg-input_login');
        this.passwordInput = this.conteiner.querySelector('.form_reg-input_password');
        this.dateInput = this.conteiner.querySelector('.form_reg-input_date');
        this.regBtn = this.conteiner.querySelector('.btn_reg');
    }
    binds(){
        this.regBtn.addEventListener('click',()=>this.register());
    }
    register(){
        let user = new User(
            this.loginInput.value,
            this.dateInput.value,
            this.passwordInput.value
        );
        this.userService.register(user).then(r=>{
            if(r.status === "error") this.registerError(r.error);
            else this.registerSuccess();
        })
    }
    registerError(text){
        alert(text);
    }
    registerSuccess(){
        this.clearForm();
        this.onRegister();
    }
    clearForm(){
            this.loginInput.value=''
            this.dateInput.value=''
            this.passwordInput.value=''
    }
}

module.exports = RegisterForm;
},{"./user":8}],7:[function(require,module,exports){
const User = require('./user')




class UserServices{
    getAll(){
        return fetch(UserServices.BASE_URL+'users')
            .then(r => r.json())
            .then(r => r.users)
            .then(us => us.map(u => User.create(u)))
    }
    register(user){
        return fetch(UserServices.BASE_URL+'register',{
            method:'POST',
            headers:{
                'Content-Type':'application/json'
            },
            body:JSON.stringify({
                login:user.login,
                password:user.password,
                date_born:user.dateBorn
            })
        }).then(r=>r.json())
    }
    login(user){
        return fetch(UserServices.BASE_URL+'login',{
            method:'POST',
            headers:{
                'Content-Type':'application/json'
            },
            body:JSON.stringify({
                login:user.login,
                password:user.password,
            })
        }).then(r=>r.json())
    }
}
UserServices.BASE_URL='https://mag-contacts-api.herokuapp.com/';

module.exports = UserServices;
},{"./user":8}],8:[function(require,module,exports){
class User{
    constructor(login,dateBorn,password){
        this.login = login;
        this.dateBorn = dateBorn;
        this.password = password;
    }
    static  create(u){
        return new User(u.login,u.date_born,":)")
    }
}

module.exports = User;
},{}]},{},[4])