let gulp = require('gulp');
let browserify = require('gulp-browserify');
let bs = require('browser-sync');


gulp.task('html',()=>
    gulp.src('src/**/*.html')
        .pipe(gulp.dest('dest/.'))
)

gulp.task('style',()=>
    gulp.src('src/**/*.css')
        .pipe(gulp.dest('dest/.'))
)

gulp.task('js', ()=>
    gulp.src('src/index.js')
        .pipe(browserify())
        .pipe(gulp.dest('dest/.'))
)

gulp.task('pre-build',gulp.parallel('html','style','js'))

gulp.task('watch',()=>{
    gulp.watch('src/**/*.css',gulp.series('style'))
    gulp.watch('src/**/*.html',gulp.series('html'))
})

gulp.task('sync',()=>{
    bs.init({
        server:{baseDir:"dest/. "}
    })
})

gulp.task('build',gulp.series('pre-build','sync'))

gulp.task('default',gulp.parallel('build','watch'))